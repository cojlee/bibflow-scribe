(function() {
    angular
        .module("bibframeEditor")
        .controller("EditorController", ["$scope", "$q", "$modal", "$log", "Store", "Configuration", "Query", "Graph", "Message", "Resolver", "Namespace", "Progress", "Property", "PredObject", "ValueConstraint", "PropertyTemplate", "ResourceTemplate", "Resource", "Profile", "ResourceStore", "TemplateStore", "Store", EditorController]);

    function EditorController($scope, $q, $modal, $log, Store, Configuration, Query, Graph, Message, Resolver, Namespace, Progress, Property, PredObject, ValueConstraint, PropertyTemplate, ResourceTemplate, Resource, Profile, ResourceStore, TemplateStore, Store) {
        // @@@ fix any data still on $scope
        $scope.inputted = {};
        $scope.results = {};
        $scope.invalid = {};
        $scope.useServices = {};
        $scope.editExisting = false; // @@@ redo this, used as a signal
        $scope.pivoting = false;
        $scope.tabs = {
            active: {}
        };
        $scope.editLoaded = false;
        $scope.searchLoading = false;

      /* $scope.ISBNclick=ISBNclick; */
        $scope.newEdit = newEdit;
        $scope.search = search;
        $scope.autocomplete = autocomplete;
        $scope.setValueFromInput = setValueFromInput;
        $scope.reset = reset;
        $scope.selectValue = selectValue;
        $scope.editLiteral = editLiteral;
        $scope.editResource = editResource;
        $scope.showResource = showResource;
        $scope.pivot = pivot;
        $scope.display = display;
        $scope.submit = submit;
        $scope.transmit = transmit;
        $scope.validate = validate;
        $scope.persist = persist;
        $scope.showRDF = showRDF;
        $scope.editFromGraph = editFromGraph;
        
        $scope.progress = Progress.getCurrent;
        $scope.messages = Message.messages;
        $scope.closeMsg = Message.removeMessage;
        $scope.initialized = Configuration.isInitialized;
        $scope.resourceOptions = Configuration.getResourceOptions;
        $scope.activeTemplate = ResourceStore.getActiveTemplate;
        $scope.current = ResourceStore.getCurrent;
        $scope.created = ResourceStore.getCreated;
        $scope.addCreated = ResourceStore.addCreated;
        $scope.cacheDropzone = ResourceStore.cacheDropzone;
        $scope.config = Configuration;
        $scope.getTemplateByID = TemplateStore.getTemplateByID;
        $scope.hasRequired = ResourceStore.hasRequired;
        $scope.setHasRequired = ResourceStore.setHasRequired;
        $scope.isLoading = ResourceStore.isLoading;
        $scope.getReferenceResourceType = TemplateStore.getReferenceResourceType;
        $scope.getTypeProperties = TemplateStore.getTypeProperties;
        $scope.dataTypes = ResourceStore.getDataTypeByID;

        initialize();

        function initialize() {
            Configuration.initialize().then(function() {
                if (Configuration.editOnLoad()) {
                    editFromGraph(Configuration.editResource());
                }
            });
        }

        function _setup(tmpl, res) {
            $scope.inputted = {};
            ResourceStore.setActiveTemplate(tmpl);
            res.setTemplate(tmpl);
            res.initialize();
        }

        /**
		 * Wipe out currently displayed resource (in $scope) and replace with
		 * pristine $scope model.
		 */
        function newEdit(resource) {
            var props, flags;
            if (!Configuration.editOnLoad() && !$scope.editLoaded) {
                $scope.tabs.active = {};
                $scope.tabs.active[resource.id] = true;
                ResourceStore.clear();
                _setup(TemplateStore.getTemplateByID(resource.id), ResourceStore.getCurrent());
            }
        }

        /**
		 * Search for local works and instances.
		 */
        /**
		 * Search for local works and instances.
		 */
        function search(typed,source) {
            var i, classes, services, idx, srv, filtered;
            if(source =="ISBN"){
            	classes = ["ISBN"];
            }else if(source =="LOCAL"){
            	classes = ["Works", "Instances"]; // maybe make this
													// configurable
            }
            services = [];
            filtered = [];

            for (i = 0; i < classes.length; i++) {
                services = services.concat(Configuration.getConfig().resourceMap[classes[i]].services);
            }
            
            angular.forEach(services, function(service, i) {
                // unclear what would happen if index was 0, do not handle
                idx = service.indexOf(":");
                if (idx > 0) {
                    srv = service.substr(0, idx);
                } else {
                    srv = service;
                }
                if (filtered.indexOf(service) < 0) {
                    filtered.push(service);
                }
            });

            $scope.searchLoading = true;
            return Query.suggest({
                q: typed,
                services: JSON.stringify(filtered.sort())
            }).$promise.then(function(res) {
                $scope.searchLoading = false;
                if(source =="ISBN"){
                	fillOutDetails(res)
                }else{
                	return res;
                }
            });            
        }

        
        /*
		 * 
		 * search by ISBN and auto fill according to book format
		 * 
		 */
        
        function fillOutDetails(data){
        	// console.log("========search data=====================");
        	// console.log(data);
        	// handle
        	var filtered = ["OCLCmulti"];

            angular.forEach(data, function(data){
           var response =  Query.suggest({
                     q: data,
                     services: JSON.stringify(filtered.sort())
                 }).$promise.then(function(res) {
                	 
                	 var actualID = "http://www.worldcat.org/oclc/"+data;
                     if( res.length >0){
                    	// console.log(res) ;
                    	var BookTemplate  = {id: "bfp:Work:Book", uri: "http://bibfra.me/vocab/marc/Books", label: "Book", sortKey: "Book", child: false};
                    	var eBookTemplate = {id: "bfp:Instance:ElectronicBook", uri: "http://bibfra.me/vocab/marc/Electronic", label: "E-book", sortKey: "E-book", child: false};
                    	var printBookTemplate = {id: "bfp:Instance:PrintBook", uri: "http://bibfra.me/vocab/marc/Print", label: "Print Book", sortKey: "Print Book", child: false}; 
                  
                    	/*
						 * 
						 * here newEdit method is for changing the tabs
						 * automatically
						 * 
						 */
                    	
                    	for (i = 0; i < res[0]['@graph'].length; i++) {
							var resultID= res[0]['@graph'][i]['@id'];
							if(resultID==actualID)
							{
								switch (res[0]['@graph'][i].bookFormat) {
								case "schema:Book":
									newEdit(BookTemplate);
									break;
								case "schema:EBook":
									newEdit(eBookTemplate);
									break;
								case "bgn:PrintBook":
									newEdit(printBookTemplate);
									break;
								default:
									// alert("there is no appropriate book
									// format , please check the response ");
									break;
								}	
							}
						}
                    	angular.forEach( ResourceStore.getCurrent().getTemplate().getOwnPropertyTemplates(), function(prop){
                       	switch (prop._property._label) {
                       	
                       	
						    case "ISBN": 
						    	if(!!(res[0]['@context'].isbn))
						    setValuetofeild(prop,res[0]['@context'].isbn); 
						    	break;
						    case "Title": 
						    	for (i = 0; i < res[0]['@graph'].length; i++) {
									var resultID= res[0]['@graph'][i]['@id'];
									if(resultID==actualID)
									{
										setValuetofeild(prop,res[0]['@graph'][i]['name']['@value']);
									}}
								break;	
						    case "Title Remainder":
						    		if(!!(res[0]['@context'].isbn))
						    		setValuetofeild(prop,res[0]['@context'].isbn);
						    	break;
						    case "Alternative Title":
						    	for (i = 0; i < res[0]['@graph'].length; i++) {
									var resultID= res[0]['@graph'][i]['@id'];
									if(resultID==actualID)
									{
										setValuetofeild(prop,res[0]['@graph'][i]['alternateName']);
									}}
								break;	
						    case "Date": 
						    	for (i = 0; i < res[0]['@graph'].length; i++) {
									var resultID= res[0]['@graph'][i]['@id'];
									if(resultID==actualID)
									{
										setValuetofeild(prop,res[0]['@graph'][i]['datePublished']);
									}}
								break;	
							case "Author":
								for (i = 0; i < res[0]['@graph'].length; i++) {
									var resultID= res[0]['@graph'][i]['@id'];
									if(resultID==actualID)
									{
										setValuetofeild(prop,res[0]['@graph'][i]['author']);
									}}
								break;	
							case "Library of Congress Classification":
								for (i = 0; i < res[0]['@graph'].length; i++) {
									var resultID= res[0]['@graph'][i]['@id'];
									if(resultID==actualID)
									{
										setValuetofeild(prop,res[0]['@graph'][i]['workExample']);
									}}
								break;	
								case "Editor":
									for (i = 0; i < res[0]['@graph'].length; i++) {
									var resultID= res[0]['@graph'][i]['@id'];
									if(resultID==actualID)
									{
										setValuetofeild(prop,res[0]['@graph'][i]['describedby']);
									}}
								break;	
								/*case "Contributor": for (i = 0; i < res[0]['@graph'].length; i++) {
									var resultID= res[0]['@graph'][i]['@id'];
									if(resultID==actualID)
									{
										setValuetofeild(prop,res[0]['@graph'][i]['datePublished']);
									}}
								break;	
						    case "Translator":for (i = 0; i < res[0]['@graph'].length; i++) {
								var resultID= res[0]['@graph'][i]['@id'];
								if(resultID==actualID)
								{
									setValuetofeild(prop,res[0]['@graph'][i]['datePublished']);
								}}
							break;	
						    case "Illustrator": for (i = 0; i < res[0]['@graph'].length; i++) {
								var resultID= res[0]['@graph'][i]['@id'];
								if(resultID==actualID)
								{
									setValuetofeild(prop,res[0]['@graph'][i]['datePublished']);
								}}
							break;	*/
						    case "Description":
						    	for (i = 0; i < res[0]['@graph'].length; i++) {
								var resultID= res[0]['@graph'][i]['@id'];
								if(resultID==actualID)
								{
									setValuetofeild(prop,res[0]['@graph'][i]['describedby']);
								}}
							break;	
						    case "Content Type": 
						    	if(!!(res[0]['@context'].isbn))
						    setValuetofeild(prop,res[0]['@context'].void); 
						    	break;
						    case "Genre": 
						    	if(!!(res[0]['@context'].isbn))
						    setValuetofeild(prop,res[0]['@context'].genre); 
						    	break;
						    case "Contents": 
						    	if(!!(res[0]['@context'].isbn))
						    setValuetofeild(prop,res[0]['@context'].void); 
						    	break;
						    case "Language": 
						    	for (i = 0; i < res[0]['@graph'].length; i++) {
									var resultID= res[0]['@graph'][i]['@id'];
									if(resultID==actualID)
									{
										setValuetofeild(prop,res[0]['@graph'][i]['inLanguage']);
									}}
								break;
						    case "Audience": 
						    	if(!!(res[0]['@context'].isbn))
						    setValuetofeild(prop,res[0]['@context'].void);
						    	break;
						    /*case "Subject": 
						    	if(!!(res[0]['@context'].isbn))
						    setValuetofeild(prop,res[0]['@context'].isbn); 
						    	break;
*/						    case "Note": 
						    	if(!!(res[0]['@context'].isbn))
						    setValuetofeild(prop,res[0]['@context'].void); 
						    	break;
						    case "Original Version Note": 
						    	if(!!(res[0]['@context'].isbn))
						    setValuetofeild(prop,res[0]['@context'].void); 
						    break;
						    case "Version": 
						    	if(!!(res[0]['@context'].isbn))
						    setValuetofeild(prop,res[0]['@context'].isbn); 
						    break;	
						    case "Edition Statement": 
						    	if(!!(res[0]['@context'].isbn))
						    setValuetofeild(prop,res[0]['@context'].isbn); 
						    break;	
						    case "Has Other Edition": 
						    	if(!!(res[0]['@context'].isbn))
						    setValuetofeild(prop,res[0]['@context'].isbn); 
						    break;	
						    case "Is Edition Of": 
						    	if(!!(res[0]['@context'].isbn))
						    setValuetofeild(prop,res[0]['@context'].isbn); 
						    break;	
						    case "Is Translation Of": 
						    	if(!!(res[0]['@context'].isbn))
						    setValuetofeild(prop,res[0]['@context'].isbn); 
						    break;	
						    case "Has Translation": 
						    	if(!!(res[0]['@context'].isbn))
						    setValuetofeild(prop,res[0]['@context'].isbn); 
						    break;
						    case "Is Based On": 
						    	if(!!(res[0]['@context'].isbn))
						    setValuetofeild(prop,res[0]['@context'].isbn); 
						    break;	
							
						    case "Has Basis": 
						    	if(!!(res[0]['@context'].isbn))
						    setValuetofeild(prop,res[0]['@context'].isbn); 
						    break;
							case "Title Proper":
								for (i = 0; i < res[0]['@graph'].length; i++) {
									var resultID= res[0]['@graph'][i]['@id'];
									if(resultID==actualID)
									{
										setValuetofeild(prop,res[0]['@graph'][i]['name']);
									}}
								break;	
							case "Edition Statement":
								for (i = 0; i < res[0]['@graph'].length; i++) {
									var resultID= res[0]['@graph'][i]['@id'];
									if(resultID==actualID)
									{
										setValuetofeild(prop,res[0]['@graph'][i]['bookEdition']);
									}}	
								break;
							case "Copyright":
								for (i = 0; i < res[0]['@graph'].length; i++) {
									var resultID= res[0]['@graph'][i]['@id'];
									if(resultID==actualID)
									{
										setValuetofeild(prop,res[0]['@graph'][i]['copyrightYear']);
									}}	
								break;
							case "Publication":
								for (i = 0; i < res[0]['@graph'].length; i++) {
									var resultID= res[0]['@graph'][i]['@id'];
									if(resultID==actualID)
									{
										if(!!(res[0]['@graph'][i]['publication']))
										setValuetofeild(prop,res[0]['@graph'][i]['publication']);
									}}		
								break;
							case "Library of Congress Call Number":
								if(!!(res[0]['@context'].library))
									setValuetofeild(prop,res[0]['@context'].library);	
									break;
								break;
								
								/*
								 * case "URI": for (i = 0; i <
								 * res[0]['@graph'].length; i++) { var resultID=
								 * res[0]['@graph'][i]['@id'];
								 * if(resultID==actualID) { for(int j=0 ; j <
								 * res[0]['@graph'][i]['url'].length ; j++ ) {
								 * setValuetofeild(prop,res[0]['@graph'][i]['url'][0]);
								 *  } } } break;
								 */
							
							case "Distribution":
								if(!!(res[0]['@context'].genre))
								setValuetofeild(prop,res[0]['@context'].rdf);	
								break;
							case "Rights Statement":
								if(!!(res[0]['@context'].genre))
								setValuetofeild(prop,res[0]['@context'].rdf);	
								break;
							case "Carrier Category":
								if(!!(res[0]['@context'].genre))
								setValuetofeild(prop,res[0]['@context'].rdf);	
								break;
							case "Media Category":
								if(!!(res[0]['@context'].genre))
								setValuetofeild(prop,res[0]['@context'].rdf);	
								break;
							case "Control Code":
								if(!!(res[0]['@context'].genre))
								setValuetofeild(prop,res[0]['@context'].rdf);	
								break;
							case "Subject":
								for (i = 0; i < res[0]['@graph'].length; i++) {
								var sublocal= res[0]['@graph'][i]['@id'];
								if(sublocal.indexOf("Topic") >-1)
								{
								setValuetofeild(prop,res[0]['@graph'][i]['name']['@value']);	
								}
								}
								break;
								default:
								break;
							}
                    		
                        });
                    	
                    	
                     }
                 });

            });
        	
        	
        }
        
        /* Sample Call */
        
       
         /*
			 * (function() {
			 * 
			 * $http.get("http://xisbn.worldcat.org/webservices/xid/isbn/0596002815?method=getMetadata&format=json&fl=*&callback=mymethod")
			 * .success(function (response) { ISBNList; $scope.ISBNList =
			 * response.records;
			 * 
			 * console.log(response);
			 * 
			 * });
			 * 
			 * })();
			 */
        
        
        
        /**
		 * Arguments are from input form, process and run to querying service.
		 */
        function autocomplete(property, typed) {
            var i, classes, single, refs, services, idx, srv, filtered;
            classes = [];
            services = [];
            filtered = [];
            refs = property.getConstraint().getReference();
            
            if (typeof refs === "object") {
                for (i = 0; i < refs.length; i++) {
                    single = TemplateStore.getReferenceResourceType(refs[i]);
                    if (classes.indexOf(single) < 0) {
                        classes.push(single);
                    }
                }
            } else {
                classes.push(TemplateStore.getReferenceResourceType(refs));
            }

            for (i = 0; i < classes.length; i++) {
                services = services.concat(Configuration.getConfig().resourceMap[classes[i]].services);
            }
            
            angular.forEach(services, function(service, i) {
                // unclear what would happen if index was 0, do not handle
                idx = service.indexOf(":");
                if (idx > 0) {
                    srv = service.substr(0, idx);
                } else {
                    srv = service;
                }
                // @@@ get away from $scope
                if ($scope.useServices[srv] && filtered.indexOf(service) < 0) {
                    filtered.push(service);
                }
            });

            ResourceStore.setLoading(property.generateFormID(), true);
            return Query.suggest({
                q: typed,
                services: JSON.stringify(filtered.sort())
            }).$promise.then(function(res) {
                ResourceStore.setLoading(property.generateFormID(), false);
               
                if(filtered.length == 1 && filtered[0].indexOf("ISBN") != -1)
                	callAnotherService(res);
                else
                return res;
                
            });
        }

       
        
        /*
         * ISBN look up and auto fill data
         * */ 
        function callAnotherService(obj){
        	
        	var filtered = ["OCLCmulti"];

            angular.forEach(obj, function(data){
           var response =  Query.suggest({
                     q: data,
                     services: JSON.stringify(filtered.sort())
                 }).$promise.then(function(res) {
                     if( res.length >0){
                    	 
                    	console.log(res) ;
                    	
                    	angular.forEach( ResourceStore.getCurrent().getTemplate().getOwnPropertyTemplates(), function(prop){
                        	
                    	/*
						 * if(prop._property._label =="Alternative Title")
						 * setValuetofeild(prop,res[0]['@context'].name);
						 */
                    		
                    		switch (prop._property._label) {
							case "Title":
								if((res[0]['@context'].name))
								setValuetofeild(prop,res[0]['@context'].name);
								
								break;
							
							case "Title Remainder":
								// setValuetofeild(prop,res[0]['@context'].name);
								/*
								 * if(!!(res[0]['@context']['about'].@id))
								 * setValuetofeild(prop,res[0]['@context']['about'].@id);
								 */	
								break;
							/*
							 * case "Alternative Title":
							 * setValuetofeild(prop,res[0]['@context'].name);
							 * break;
							 * 
							 * 
							 * case "Date":
							 * setValuetofeild(prop,res[0]['@context'].name);
							 * break;
							 */
							case "Library of Congress Classification":
								setValuetofeild(prop,res[0]['@context'].library);	
								break;
								/*
								 * case "Editor":
								 * setValuetofeild(prop,res[0]['@context'].name);
								 * break;
								 * 
								 * case "Contributor":
								 * setValuetofeild(prop,res[0]['@context'].name);
								 * break;
								 * 
								 * case "Translator":
								 * setValuetofeild(prop,res[0]['@context'].name);
								 * break; case "Illustrator":
								 * setValuetofeild(prop,res[0]['@context'].name);
								 * break; case "Description":
								 * setValuetofeild(prop,res[0]['@context'].name);
								 * break; case "Content Type":
								 * setValuetofeild(prop,res[0]['@context'].name);
								 * break;
								 */
							case "Genre":
								if(!!(res[0]['@context'].genre))
								setValuetofeild(prop,res[0]['@context'].genre);	
								break;
							/*
							 * case "Contents":
							 * setValuetofeild(prop,res[0]['@context'].name);
							 * break; case "Language":
							 * setValuetofeild(prop,res[0]['@context'].name);
							 * break; case "Audience":
							 * setValuetofeild(prop,res[0]['@context'].name);
							 * break;
							 */
							case "Subject":
								for (i = 0; i < res[0]['@graph'].length; i++) {
									
									var sublocal= res[0]['@graph'][i]['@id'];
								if(sublocal.indexOf("Topic") >-1)
								{
								setValuetofeild(prop,res[0]['@graph'][i]['name']['@value']);	
								}
								}
								break;
								/*
								 * case "Note":
								 * setValuetofeild(prop,res[0]['@context'].name);
								 * break; case "Original Version Note":
								 * setValuetofeild(prop,res[0]['@context'].name);
								 * break; case "Version":
								 * setValuetofeild(prop,res[0]['@context'].name);
								 * break; case "Edition Statement":
								 * setValuetofeild(prop,res[0]['@context'].name);
								 * break; case "Has Other Edition":
								 * setValuetofeild(prop,res[0]['@context'].name);
								 * break; case "Is Edition Of":
								 * setValuetofeild(prop,res[0]['@context'].name);
								 * break; case "Is Translation Of":
								 * setValuetofeild(prop,res[0]['@context'].name);
								 * break; case "Has Translation":
								 * setValuetofeild(prop,res[0]['@context'].name);
								 * break; case "Is Based On":
								 * setValuetofeild(prop,res[0]['@context'].name);
								 * break; case "Has Basis":
								 * setValuetofeild(prop,res[0]['@context'].name);
								 * break; case "URI":
								 * setValuetofeild(prop,res[0]['@context'].name);
								 * break; case "Note":
								 * setValuetofeild(prop,res[0]['@context'].name);
								 * break; case "Note":
								 * setValuetofeild(prop,res[0]['@context'].name);
								 * break; case "Note":
								 * setValuetofeild(prop,res[0]['@context'].name);
								 * break; case "Note":
								 * setValuetofeild(prop,res[0]['@context'].name);
								 * break; case "Note":
								 * setValuetofeild(prop,res[0]['@context'].name);
								 * break; case "Note":
								 * setValuetofeild(prop,res[0]['@context'].name);
								 * break;
								 */	default:
								break;
							}
                    		
                        });
                    	
                    	
                     }
                 });

            });
        }
        
        
        /**
		 * Convenience method
		 */
        function setValuetofeild(prop, text) {
            // @@@ rewrite to do without $scope
            if (!$scope.editExisting) {
                $scope.invalid[prop.generateFormID()] = false;
                ResourceStore.getCurrent().addPropertyValue(prop, text);
            }
        }
        
        
        /**
		 * Fires when a dropdown item is selected
		 */
        
        function selectValue(property, selection, created) {
            var seen = false, objType;
            objType = property.getType();
            angular.forEach(ResourceStore.getCurrent().getPropertyValues(property), function(val) {
                if (selection.uri === val.getValue()) {
                    seen = true;
                }
            });
            if (typeof created === "undefined") {
                created = false;
            }
            if (!seen) {
                ResourceStore.getCurrent().addPropertyValue(property, new PredObject(selection.label, selection.uri, objType, created));
            }
            if(property._fromResource == "bfp:Work:Book" && property._property._label == "Title"){
            	somemethod(property, selection);
            }
        }
        
        /* title lookup and auto fill for book and e-book */
        
        
        	function autoFillData(res,tmpl,refId){
        		var actualURI= 'http://www.worldcat.org/oclc/'+refId;
        		var props = tmpl._template.getOwnPropertyTemplates();
        	angular.forEach( props, function(prop){
                	console.log("============response from title search========================"+res);
            		
            		switch (prop._property._label) {
            		case "ISBN":
						if(!!(res[0]['@context'].isbn))
							setValuetofeild(prop,res[0]['@context'].isbn);
						break;
            		case "Title Remainder":
						if(!!(res[0]['@context'].bgn))
							setValuetofeild(prop,res[0]['@context'].bgn);
						break;
					/*
					 * case "Alternative Title":
					 * setValuetofeild(prop,res[0]['@context'].name); break;
					 */
					case "Date":
						for (i = 0; i < res[0]['@graph'].length; i++) {
							var resultURI= res[0]['@graph'][i]['@id'];
							if(resultURI==actualURI)
							{
								setValuetofeild(prop,res[0]['@graph'][i].datePublished);
								
							}
						}
						
						/*if(!!(res[0]['@context'].datePublished ))
						setValuetofeild(prop,res[0]['@context'].datePublished);	*/
						break;
					case "Author":
						if(!!( res[0]['@context']['author']['@id']))
						setValuetofeild(prop,res[0]['@context']['author']['@id']);	
						break;
						
					case "Library of Congress Classification":
						if(!!( res[0]['@context'].library))
						setValuetofeild(prop,res[0]['@context'].library);	
						break;
						
						/*
						 * case "Editor": if(!!( ))
						 * setValuetofeild(prop,res[0]['@context'].name); break;
						 * case "Contributor": if(!!( ))
						 * setValuetofeild(prop,res[0]['@context'].name); break;
						 * case "Translator": if(!!( ))
						 * setValuetofeild(prop,res[0]['@context'].name); break;
						 * case "Illustrator": if(!!( ))
						 * setValuetofeild(prop,res[0]['@context'].name); break;
						 */
						
					case "Description":
						for (i = 0; i < res[0]['@graph'].length; i++) {
							var resultURI= res[0]['@graph'][i]['@id'];
							if(resultURI==actualURI)
							{
								setValuetofeild(prop,res[0]['@graph'][i].description['@value']);
								
							}
						}break;
						/*
						 * case "Content Type": if(!!( ))
						 * setValuetofeild(prop,res[0]['@context'].name); break;
						 */
					case "Genre":
						for (i = 0; i < res[0]['@graph'].length; i++) {
							var resultURI= res[0]['@graph'][i]['@id'];
							if(resultURI==actualURI)
							{
								setValuetofeild(prop,res[0]['@graph'][i].genre['@value']);
								
							}
						}
						break;
					/*
					 * case "Contents": if(!!( ))
					 * setValuetofeild(prop,res[0]['@context'].name); break;
					 */
					case "Language":
						for (i = 0; i < res[0]['@graph'].length; i++) {
							var resultURI= res[0]['@graph'][i]['@id'];
							if(resultURI==actualURI)
							{
								setValuetofeild(prop,res[0]['@graph'][i].genre['@language']);
								
							}
						}
						break;
						
						/*
						 * case "Audience": if(!!( ))
						 * setValuetofeild(prop,res[0]['@context'].name); break;
						 */
					case "Subject":
						for (i = 0; i < res[0]['@graph'].length; i++) {
							
							var sublocal= res[0]['@graph'][i]['@id'];
						if(sublocal.indexOf("Topic") >-1)
						{
							   var n=sublocal.lastIndexOf('/');
							   var label = sublocal.substring(n + 1);
							   var re = new RegExp('_', 'g');
							   var result = label.replace(re,'-');
						//setValuetofeild(prop,res[0]['@graph'][i]['name']['@value']);
							setValuetofeild(prop,result);
						}
						}
						break;
						/*
						 * case "Note": if(!!( ))
						 * setValuetofeild(prop,res[0]['@context'].name); break;
						 * case "Original Version Note": if(!!( ))
						 * setValuetofeild(prop,res[0]['@context'].name); break;
						 * case "Version": if(!!( ))
						 * setValuetofeild(prop,res[0]['@context'].name); break;
						 * case "Edition Statement": if(!!( ))
						 * setValuetofeild(prop,res[0]['@context'].name); break;
						 * case "Has Other Edition": if(!!( ))
						 * setValuetofeild(prop,res[0]['@context'].name); break;
						 * case "Is Edition Of": if(!!( ))
						 * setValuetofeild(prop,res[0]['@context'].name); break;
						 * case "Is Translation Of": if(!!( ))
						 * setValuetofeild(prop,res[0]['@context'].name); break;
						 * case "Has Translation": if(!!( ))
						 * setValuetofeild(prop,res[0]['@context'].name); break;
						 * case "Is Based On": if(!!( ))
						 * setValuetofeild(prop,res[0]['@context'].name); break;
						 * case "Has Basis": if(!!( ))
						 * setValuetofeild(prop,res[0]['@context'].name); break;
						 * case "URI": if(!!( ))
						 * setValuetofeild(prop,res[0]['@context'].name); break;
						 * case "Note": if(!!( ))
						 * setValuetofeild(prop,res[0]['@context'].name); break;
						 * case "Note": if(!!( ))
						 * setValuetofeild(prop,res[0]['@context'].name); break;
						 * case "Note": if(!!( ))
						 * setValuetofeild(prop,res[0]['@context'].name); break;
						 * case "Note": if(!!( ))
						 * setValuetofeild(prop,res[0]['@context'].name); break;
						 * case "Note": if(!!( ))
						 * setValuetofeild(prop,res[0]['@context'].name); break;
						 * case "Note": if(!!( ))
						 * setValuetofeild(prop,res[0]['@context'].name); break;
						 */	default:
						break;
					}	
                })
        	if (tmpl._relation !== null) {
        		autoFillData(res,tmpl._relation);
        	}
        	
        	}
        
        	
        	
        	
        	/*second service call for title search*/
        function somemethod(property, selection){
        	
        	var refId = selection.refId;
        	
        	// call service
        	var filtered = ["oclcSingle"];// your service name
        	 Query.suggest({
                 q: refId,
                 services: JSON.stringify(filtered.sort())
             }).$promise.then(function(res) {
            	 console.log("*********************oclcSingle****************"+res);
            	 autoFillData(res,ResourceStore.getCurrent(), refId);
            	 
             	
             });
        	
        }

        /**
		 * Convenience method
		 */
        function setValueFromInput(prop, inputs) {
            // @@@ rewrite to do without $scope
            if (!$scope.editExisting) {
                $scope.invalid[prop.generateFormID()] = false;
                ResourceStore.getCurrent().addPropertyValue(prop, inputs[prop.generateFormID()]);
                inputs[prop.generateFormID()] = '';
            }
        }

        /**
		 * Wipe out $scope form data as if no input was made.
		 */
        function reset(formScope, formName) {
            ResourceStore.reset();
            $scope.inputted = {};
            formScope[formName].$setPristine();
        }

        /**
		 * Open a modal dialog for user to change a literal value.
		 */
        function editLiteral(property, value) {
            var modal = $modal.open({
                templateUrl: "edit-literal.html",
                controller: "EditLiteralController",
                resolve: {
                    literal: function() {
                        return value.getValue();
                    },
                    property: function() {
                        return property;
                    },
                    ResourceStore: function() {
                        return ResourceStore;
                    }
                }
            });

            modal.result.then(function(newValue) {
                var objs, target, mod;
                objs = ResourceStore.getCurrent().getPropertyValues(property);
                angular.forEach(objs, function(obj, idx) {
                    if (obj.getValue() === value.getValue()) {
                        target = idx;
                    };
                });
                mod = objs[target];
                mod.setLabel(newValue);
                mod.setValue(newValue);
            });
        }

        /**
		 * Shortcut for pivot call. Choose created resoure and matching template
		 * before pivoting.
		 */
        function editResource(property, value) {
            var toEdit, ref;
            angular.forEach(ResourceStore.getCreated(), function(val) {
                if (val.getID() === value.getValue()) {
                    toEdit = val;
                }
            });

            pivot(property, null, toEdit);
        }

        /**
		 * Show a model dialog for an external URI using Resolver-queried RDF -
		 * very similar to exported RDF dialog, perhaps combine.
		 */
        function showResource(val) {
            if (val.isResource()) {
                Resolver.resolve({"uri": val.getValue()}).$promise.then(function(data) {
                    $modal.open({
                        templateUrl: "show-resource.html",
                        controller: "ShowResourceController",
                        windowClass: "show-resource",
                        resolve: {
                            rdf: function() {
                                return data.raw;
                            },
                            label: function() {
                                return val.getLabel();
                            },
                            uri: function() {
                                return val.getValue();
                            }
                        }
                    });
                }).catch(function(data) {
                    $modal.open({
                        templateUrl: "show-resource.html",
                        controller: "ShowResourceController",
                        windowClass: "show-resource",
                        resolve: {
                            rdf: function() {
                                return "No additional data could be found.";
                            },
                            label: function() {
                                return val.getLabel();
                            },
                            uri: function() {
                                return val.getValue();
                            }
                        }
                    });
                });
            }
        }

        /**
		 * Open up new input form in modal dialog to fill in a sub-resource of
		 * the main form. Takes from and modifes $scope.
		 */
        function pivot(property, ref, toEdit) {
            var modal, res, doInit;

            if (typeof toEdit === "undefined") {
                if (typeof ref === "undefined") {
                    ref = property.getConstraint().getReference();
                }

                res = TemplateStore.getTemplateByID(ref);

                toEdit = new Resource(Configuration.getConfig().idBase, res);
                doInit = true;
            } else {
                res = toEdit.getTemplate();
                doInit = false;
            }

            ResourceStore.pivot(toEdit);

            modal = $modal.open({
                templateUrl: "pivot.html",
                controller: "SubResourceController",
                windowClass: "pivot",
                scope: $scope,
                resolve: {
                    $modal: function() {
                        return $modal;
                    },
                    template: function() {
                        return res;
                    },
                    doInitialization: function() {
                        return doInit;
                    }
                }
            });
            
            modal.result.then(function() {
                var r = ResourceStore.pivotDone();
                selectValue(property, {"label": "[created]", "uri": r.getID()}, true);
            }, function() {
                ResourceStore.pivotDone();
            });
        }
        
        /**
		 * WAS export, but that's a reserved word. Shortcut for displaying.
		 */
        function display() {
            transmit("export");
        }

        /**
		 * Shortcut for saving.
		 */
        function submit() {
            transmit("save");
        }
        
        /**
		 * Depending on flag, shows RDF to user or persists N3 to store.
		 */
        function transmit(flag) {
            var i, invalid, names;
            invalid = validate(ResourceStore.getActiveTemplate(), $scope.invalid);
            if (invalid.length === 0) {
                if (flag === "export") {
                    showRDF();
                } else if (flag === "save") {
                	//var abc = angular.toJson(ResourceStore.getCurrent());
                	//console.log("this is from where you never expect : ====  : "+ResourceStore.getCurrent().toJson(ResourceStore.getCreated()));
                   console.log("resource get created : "+angular.toJson( ResourceStore.getCreated()[0]));
                	persist(ResourceStore.getCurrent().toN3(ResourceStore.getCreated()));
                }
            } else {
                names = "";
                for (i = 0; i < invalid.length; i++) {
                    names += "<li>" + invalid[i].getProperty().getLabel() + "</li>";
                }
                Message.addMessage("Please fill out all required properties before " + ((flag === "save") ? "saving" : "exporting") + ": <ul>" + names + "</ul>", "danger");
            }
        }

        /**
		 * Checks $scope current work for whether mandatory-ness is met.
		 */
        function validate(tmpl, sc) {
            var props, invalid;
            props = tmpl.getPropertyTemplates();
            invalid = [];
            angular.forEach(props, function(prop) {
                if (prop.isRequired() && ResourceStore.getCurrent().getPropertyValues(prop).length === 0) {
                    invalid.push(prop);
                    sc[prop.generateFormID()] = true;
                }
            });
            return invalid;
        }

        /**
		 * Takes N3 string and persists it to local and backing stores.
		 */
        function persist(n3) {
            Graph.loadResource(null, n3).then(function() {
                Store.new(null, {"n3": n3}).$promise.then(function(resp) {
                    if (resp.success) {
                        Message.addMessage("Saved!", "success");
                    } else {
                        Message.addMessage("Failed to save!", "danger")
                    }
                });
            }, function() {
                // console.log("not loaded");
            });
        }

        /**
		 * Open modal dialog to show (stored) string to user.
		 */
        function showRDF() {
            $modal.open({
                templateUrl: "export.html",
                controller: "ExportController",
                windowClass: "export",
                resolve: {
                    rdf: function() {
                        return ResourceStore.getCurrent().toRDF(ResourceStore.getCreated());
                    }
                }
            });
        }

        /**
		 * Utility function to fill out a Resource from a local browser store,
		 * only retrieves triples with uri argument as subject.
		 */
        function editFromGraph(uri) {
            var existq = "ASK { <" + uri + "> ?p ?o }",
                typeq = "SELECT ?o WHERE { <" + uri + "> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> ?o }",
                tmpl;

            Graph.execute(uri, existq, Graph.DATA).then(function(resp) {
                return resp[1];
            }).then(function(inGraph) {
                if (!inGraph) {
                    // If not in local store, retrieve from backing store
                    Store.get({}, {s: uri}).$promise.then(function(triples) {
                        Graph.loadResource(uri, triples.n3).then(function() {
                            _loadFromGraph(uri).catch(function() {
                                Message.addMessage("Cannot edit " + uri, "danger");
                            });
                        });
                    });
                } else {
                    _loadFromGraph(uri).catch(function() {
                        Message.addMessage("Cannot edit " + uri, "danger");
                    });
                }
            }).catch(function() {
                Message.addMessage("Not in backing store: " + uri, "danger");
            });
        }

        function _loadFromGraph(uri, sub) {
            var rels, fullq, typeq, relsq, res, defer, tmpl;
            
            if (typeof sub === "undefined") {
                sub = false;
            }
            defer = $q.defer();
            rels = [];
            angular.forEach(Configuration.getConfig().relations, function(v, p) {
                rels.push(Graph.BF + p);
            });
            
            res = new Resource(null, null);
            typeq = "SELECT * WHERE { <" + uri + "> a ?o }";
            relsq = "SELECT * WHERE { <" + uri + "> ?p ?o . ?o a ?t . FILTER(" + rels.map(function(a) { return "?p = <" + a + ">"; }).join(" || ") + ") }";
            fullq = "SELECT * WHERE { <" + uri + "> ?p ?o  }";
            
            $q.all([
                Graph.execute(res, typeq, Graph.DATA).then(function(response) {
                    if (response[1].length > 0) {
                        return response[1][0].o.value;
                    } else {
                        return null;
                    }
                }),
                Graph.execute(res, relsq, Graph.DATA).then(function(response) {
                    if (response[1].length > 0) {
                        return {
                            p: response[1][0].p.value,
                            o: response[1][0].o.value,
                            t: response[1][0].t.value
                        };
                    } else {
                        return null;
                    }
                })
            ]).then(function(response) {
                var prop, ids, subids;
                if (response[0] !== null) {
                    res.setID(uri);
                    ids = TemplateStore.identifiersFromClassID(response[0]);
                    if (ids !== null && ids.length === 1) {
                        tmpl = TemplateStore.getTemplateByID(ids[0]);
                        if (!sub) {
                            _setup(tmpl, res);
                            $scope.tabs.active[ids[0]] = true;
                        } else {
                            res.setTemplate(tmpl);
                            res.initialize();
                        }
                    }
                    if (response[1] !== null) {
                        prop = Namespace.extractNamespace(response[1].p);
                        if (typeof tmpl === "undefined") {
                            subids = TemplateStore.identifiersFromClassID(response[1].t);
                            if (subids !== null && subids.length === 1) {
                                res.getRelation().setTemplate(TemplateStore.getTemplateByID(subids[0]));
                                res.getRelation().initialize();
                                angular.forEach(ids, function(id) {
                                    var candidate;
                                    candidate = TemplateStore.getTemplateByID(id);
                                    if (candidate.getRelationType() === prop.term) {
                                        tmpl = candidate;
                                        if (!sub) {
                                            _setup(tmpl, res);
                                            $scope.tabs.active[id] = true;
                                        }  else {
                                            res.setTemplate(tmpl);
                                            res.initialize();
                                        }
                                    }
                                });
                            }
                        }
                        _loadFromGraph(response[1].o, true).then(function(subres) {
                            res.setRelation(subres);
                        });
                    }
                    Graph.execute(res, fullq, Graph.DATA).then(function(triples) {
                        angular.forEach(triples[1], function(triple) {
                            if (tmpl.hasProperty(triple.p.value)) {
                                if (triple.o.token === "literal") {
                                    res.addPropertyValue(
                                        tmpl.getPropertyByID(triple.p.value),
                                        new PredObject(
                                            triple.o.value,
                                            triple.o.value,
                                            PropertyTemplate.LITERAL,
                                            true
                                        )
                                    );
                                } else {
                                    res.addPropertyValue(
                                        tmpl.getPropertyByID(triple.p.value),
                                        new PredObject(
                                            triple.o.value,
                                            triple.o.value,
                                            PropertyTemplate.RESOURCE,
                                            false
                                        )
                                    );
                                }
                            }
                        });
                        if (!sub) {
                            ResourceStore.setCurrent(res);
                            $scope.editLoaded = true;
                        }
                        defer.resolve(res);
                    });
                }
            });
            return defer.promise;
        }
        
    }
})();
